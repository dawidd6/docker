docker.io (18.09.1+dfsg1-7.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Hideki Yamane ]
  * upstream site moved to mobyproject.org

  [ Arnaud Rebillout ]
  * Add patch for CVE-2018-15664 (Closes: #929662).

 -- Shengjing Zhu <zhsj@debian.org>  Sun, 23 Jun 2019 01:25:10 +0800

docker.io (18.09.1+dfsg1-7) unstable; urgency=medium

  * Add patch to revert using iptables-legacy (Closes: #921600).

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Mon, 13 May 2019 09:34:45 +0700

docker.io (18.09.1+dfsg1-6) unstable; urgency=medium

  * Add patch to fix Debian security presence check (Closes: #925224).

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Tue, 16 Apr 2019 09:56:17 +0700

docker.io (18.09.1+dfsg1-5) unstable; urgency=medium

  * Install "containerd-shim" as "docker-containerd-shim" (Closes: #920935).
  * Update containerd-name patch.

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Sat, 02 Feb 2019 10:00:35 +1100

docker.io (18.09.1+dfsg1-4) unstable; urgency=medium

  * Updated "containerd" executable name patch;
    renamed "containerd-shim" executable (Closes: #920597).

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 28 Jan 2019 10:16:28 +1100

docker.io (18.09.1+dfsg1-3) unstable; urgency=medium

  * New patch to fix name of the "containerd" executable (Closes: #920597).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 27 Jan 2019 23:43:53 +1100

docker.io (18.09.1+dfsg1-2) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Standards-Version: 4.3.0.
  * Upload to unstable.

  [ Arnaud Rebillout ]
  * Bump runc requirement to 1.0.0~rc6.
  * Add patch to skip flaky test.
  * Tidy up patches.

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Sat, 26 Jan 2019 10:58:39 +1100

docker.io (18.09.1+dfsg1-1) experimental; urgency=medium

  * New upstream release [January 2019].
  * Remove obsolete patches, refresh remaining ones.
  * New notable patches:
    - build against the runc debian package.
    - build against google-grpc 1.11.
    - attempt to fix mips build.
    - disable a test file that fails to build (known issue upstream).
  * Remove various build dependencies, add new ones.
  * Bump some build dependencies:
    - golang-github-coreos-bbolt-dev (>= 1.3.1-coreos.5-3~).
  * Vendor some build dependencies:
    - docker/licensing (no debian package, no upstream release).
    - golang-github-spf13-cobra/pflag-dev (docker has internal fork).

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Tue, 22 Jan 2019 19:48:15 +1100

docker.io (18.06.1+dfsg1-3) unstable; urgency=medium

  * Import upstream patch to use iptables-legacy (Closes: #911808).
  * Un-vendor opencontainers-runtime-tools.
  * Import numerous patches from upstream for go 1.11.

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Thu, 17 Jan 2019 15:37:54 +1100

docker.io (18.06.1+dfsg1-2) unstable; urgency=medium

  * Tighten versioned dependency on "runc".
  * dev: install "libnetwork/ipamutils".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 16 Sep 2018 13:21:33 +1000

docker.io (18.06.1+dfsg1-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * New upstream release [August 2018].
  * Upload to unstable (Closes: #906999).

  [ Arnaud Rebillout ]
  * Cleanup /etc/init/docker.conf (Closes: #907455)

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 11 Sep 2018 14:03:46 +1000

docker.io (18.06.0+dfsg1-1) experimental; urgency=medium

  * New upstream release [July 2018].

  [ Arnaud Rebillout ]
  * get-orig-source: print the list of directories vendored by upstream.

  [ Dmitry Smirnov ]
  * README.source: noted duration of upstream support.
  * README.Debian: added note about restart dilemma.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 10 Aug 2018 19:07:41 +1000

docker.io (18.03.1+dfsg1-6) unstable; urgency=medium

  * Removed obsolete "golang-ed25519-dev" from Build-Depends.
  * Standards-Version: 4.1.5.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 11 Jul 2018 20:15:24 +1000

docker.io (18.03.1+dfsg1-5) unstable; urgency=medium

  * New upstream patch to prevent needless calls to `pass` (Closes: #902258).
  * Do not automatically restart daemon on upgrade (Closes: #786724).
  * Recommends += "needrestart".

    "needrestart" prompts to restart "docker" daemon on upgrade. This way
    running containers won't be killed on upgrade until user choses to
    restart Docker.
    Not restarting Docker on upgrade may break CLI when it disagrees with
    running daemon regarding API version.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 02 Jul 2018 14:56:46 +1000

docker.io (18.03.1+dfsg1-4) unstable; urgency=medium

  * Suggests += "e2fsprogs, xfsprogs" (Closes: #887222).
  * rules: fixed test failure on binary-indep build (Closes: #902206).
    Thanks, Santiago Vila.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 24 Jun 2018 23:22:51 +1000

docker.io (18.03.1+dfsg1-3) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Replaced upstream SysV init file with an improved one.
      * --remove-pidfile on stop (Closes: #764921)
      * don't fail when removed (Closes: #841282)
      * fixed exit status:
          + don't fail to stop when already stopped
          + don't fail to start when already started
      * removed useless check_init()
    Thanks, Sam Morris.
  * postinst: create "docker" group when needed (Closes: #821078).
  * README.source: added comment to clarify upstream version numbering.
    Thanks, Tianon Gravi
  * README.source: added link describing upstream life cycle & release
    policy. Thanks, Tianon Gravi.

  [ Arnaud Rebillout ]
  * Added myself to uploaders.
  * Bumped compat to 11 to allow installling the systemd socket
    file automatically with dh_installsystemd.
  * Installed systemd socket through dh_installsystemd.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 21 Jun 2018 21:27:32 +1000

docker.io (18.03.1+dfsg1-2) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Upload to unstable.
  * Un-bundle "googleapis-gnostic-dev" and "docker-notary-dev".
  * Removed "golang-github-hashicorp-consul-dev" from Build-Depends.

  [ Arnaud Rebillout ]
  * Fix bash completion install.
  * d/control: depend on golang-any.
  * d/README.source: re-write part about docker-ce upstream workflow.
  * Set required version for imdario-mergo and hashicorp-memberlist.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 20 Jun 2018 23:40:47 +1000

docker.io (18.03.1+dfsg-1) experimental; urgency=medium

  * New upstream release [April 2018].
  * rules:
    + properly pass daemon version.
    + re-work override_dh_auto_configure with important fixes.
  * New patch to disable unreliable TestAdapterReadLogs.
  * Use packaged "tini", don't build it.
    Thanks, Arnaud Rebillout.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 19 Jun 2018 13:43:57 +1000

docker.io (17.12.1+dfsg-4) experimental; urgency=medium

  * golang-github-docker-docker-dev:
    + install only selected "libnetwork" components.
    + install missing "docker/cli" components.
    + install "docker/docker/cli".

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Jun 2018 19:32:51 +1000

docker.io (17.12.1+dfsg-3) experimental; urgency=medium

  * Removed versioning from -dev Breaks/Replaces: libnetwork-dev
    (Closes: #901694).
  * Build with consistent tags "apparmor seccomp selinux ambient"
    (Closes: #901743).
    Thanks, Laurent Bigonville.
  * New patch to fix FTBFS on mips* architectures.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 18 Jun 2018 12:05:10 +1000

docker.io (17.12.1+dfsg-2) experimental; urgency=medium

  * New patch to disable TestGetRootUIDGID, failing in sbuild.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 16 Jun 2018 21:31:51 +1000

docker.io (17.12.1+dfsg-1) experimental; urgency=medium

  * Team upload.
  * New upstream release [February 2018] (Closes: #850753).
  * Restart on upgrade, like most daemons (Closes: #792327).
    Docker's upgrade tip from 17.12.0 release notes:
      "You must stop all containers and plugins BEFORE upgrading".
  * New multi-upstream-tarball (MUT) layout, building all docker components
    (containerd, libnetwork, swarmkit) at once;
    incorporated binaries of docker-containerd and libnetwork.
  * docker-dev to provide libnetwork-dev (a part of Docker).
  * New patches to build on go-1.10; build with latest Go compiler.
  * Declared myself as Maintainer.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 16 Jun 2018 20:05:48 +1000

docker.io (1.13.1~ds3-4) unstable; urgency=medium

  * Build with go-1.7 as later versions cause [archive,tarsum] failures
    (Closes: #901317).
  * Re-enabled [archive,tarsum] tests.
  * Build-Depends:
    = golang-1.7-go | golang-go (>= 2:1.6~)

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 12 Jun 2018 00:02:27 +1000

docker.io (1.13.1~ds3-3) unstable; urgency=medium

  * Removed invalid team email from Uploaders (Closes: #899285).
  * B-D: "libbtrfs-dev | btrfs-progs (<< 4.16.1~)" (Closes: #898876).
    Thanks, Dimitri John Ledkov.
  * (Build-)Depends:
    - removed unused "golang-github-aanand-compose-file-dev".
    - removed needless versioning and unknown alternatives.
  * dev: install "runconfig" (used by "github.com/aanand/compose-file").
  * repack.sh: use correct compression type, depending on file name.
  * watch file to version 4; updated "repack.sh".

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 10 Jun 2018 19:49:42 +1000

docker.io (1.13.1~ds3-2) unstable; urgency=medium

  * Team upload.
  * Install -dev files from build directory.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 09 Jun 2018 20:20:53 +1000

docker.io (1.13.1~ds3-1) unstable; urgency=medium

  * Team upload.

  [ Tianon Gravi ]
  * Remove gccgo support.
    Removed upstream in commit eda90f63446253f97d2011926555306f2417d208
    (https://github.com/moby/moby/pull/25978)
  * Update upstream-version-gitcommits with more upstream versions

  [ Dmitry Smirnov ]
  * New patch to fix CVE-2017-16539 (Closes: #900140).
  * New patch to remove 10 seconds delay on purge (Closes: #853258).
  * debhelper to version 11; compat to version 10.
  * copyright format URL to HTTPS; bump copyright years.
  * Standards-Version: 4.1.4.
  * Vcs URLs to Salsa.
  * Included "cliconfig" to -dev package (used by "gitlab-runner").
  * Included "reference" and "registry" into -dev package (used by "nomad").
  * Removed obsolete "golang-github-docker-engine-api-dev" from Build-Depends.
  * Use more private libraries to fix build and break circular dependencies:
    + github.com/docker/swarmkit
    + github.com/docker/libnetwork
    + github.com/docker/go-events
    + github.com/docker/go-metrics
  * Removed Upstart .conf file.
  * rules:
    + better clean, remove generated file(s).
    + fixed "sirupsen/logrus" imports.
    + DH_GOLANG_GO_GENERATE = 1

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 09 Jun 2018 14:50:13 +1000

docker.io (1.13.1~ds2-3) unstable; urgency=medium

  * Switch from "runc" to "docker-runc" and "containerd" to
    "docker-containerd", removing prefix-removing patch
    (Closes: #877329, #877892)
  * Update patches, especially test-disablers

 -- Tianon Gravi <tianon@debian.org>  Sat, 14 Oct 2017 08:58:11 -0700

docker.io (1.13.1~ds1-2) unstable; urgency=medium

  * Make test suite pass by using assorted patches to fix or disable
    tests that don't work under pbuilder. (Closes: #858269)
  * Suppress some unfixable Lintian warnings.
  * Verify CVE-2016-9962 is fixed. (Closes: #850952)

 -- Tim Potter <tpot@hpe.com>  Wed, 07 Jun 2017 11:43:14 +1000

docker.io (1.13.1~ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Tim Potter <tpot@hpe.com>  Wed, 24 May 2017 11:44:10 +1000

docker.io (1.13.0~ds1-3) unstable; urgency=medium

  * Add api and client directories to dev package.

 -- Tim Potter <tpot@hpe.com>  Mon, 24 Apr 2017 16:02:32 +1000

docker.io (1.13.0~ds1-2) unstable; urgency=medium

  * Re-enable logfiles.com logging support after upstream license
    change.
  * Run nuke-graph-directory.sh using bash instead of regular sh.
  * Fix dockerd location for sysvinit and upstart scripts. (Closes: #858249)

 -- Tim Potter <tpot@hpe.com>  Tue, 28 Mar 2017 15:41:55 +1100

docker.io (1.13.0~ds1-1) experimental; urgency=medium

  [ Paul Tagliamonte ]
  * Remove myself as maintainer, and swap out tpot. Sadly, these days, I'm
    mostly just in the way, and not actually helping all that much with
    the Docker packaging. My last upload was basically forever ago, and
    tianon and tpot have been doing all the work since than. As such, I'm
    going to make an unilateral executive decision to tell everyone who
    listens to actually just listen to tpot. I plan to continue to be around in
    the form of cruft and chaos monkey. You should also listen to tianon.

  [ Tianon Gravi ]
  * Update basic-smoke test with "set -x" for debuggability and proper Depends
  * Build from within GOPATH so Go packages are resolved properly
  * Split "dh_auto_build-arch" from "dh_auto_build-indep"
  * Update "debian/watch" to use "uscan.tianon.xyz" so older versions are still
    easily fetchable without excess work outside uscan
  * Fix d/copyright text about Apache version 2.0 being in
    "/usr/share/common-licenses/GPL-2" (Closes: #835440); thanks cascardo!
  * Add Tim Potter to Uploaders ♥
  * Add a bit more formatting to README.Debian (and a short intro to explain
    what kinds of things this file includes)
  * Add an explicit note about "systemd.legacy_systemd_cgroup_controller=yes"
    in README.Debian (Closes: #843530)
  * Add explicit new "golang-golang-x-oauth2-google-dev" package to Depends

  [ Tim Potter ]
  * Add missing "golang-github-docker-go-events-dev" B-D (Closes: #850793)
  * New upstream version.
  * Refresh patches and remove obsolete ones.
  * Remove logentries.com log driver as upstream module is unlicensed.

 -- Tianon Gravi <tianon@debian.org>  Fri, 19 Aug 2016 12:52:55 -0700

docker.io (1.11.2~ds1-6) unstable; urgency=medium

  [ Tianon Gravi ]
  * Add DEP-3 headers for "skip-racy-unit-tests.patch"
  * Add a note about "check-config.sh" to README.Debian; thanks Tincho!
  * Add "docker-doc" to Suggests (Closes: #831748); thanks Ben!
  * Remove "lxc" from Suggests (no longer a supported execution backend)

  [ Nicolas Braud-Santoni ]
  * Fix /etc/docker permissions (Closes: #831324)

 -- Tianon Gravi <tianon@debian.org>  Wed, 20 Jul 2016 16:34:52 -0700

docker.io (1.11.2~ds1-5) unstable; urgency=medium

  * Skip racy "TestRunCommandWithOutputAndTimeoutKilled" during build (see also
    https://github.com/docker/docker/issues/22965)

 -- Tianon Gravi <tianon@debian.org>  Tue, 12 Jul 2016 07:46:35 -0700

docker.io (1.11.2~ds1-4) unstable; urgency=medium

  [ Tianon Gravi ]
  * Add new script to generate Build-Depends based on "go list" instead of
    "hack/vendor.sh" (and update Build-Depends using it)
  * Update "/etc/default/docker" text to aggressively discourage use, linking to
    upstream's documentation for the recommended alternatives
    ("/etc/docker/daemon.json" and systemd drop-ins)
  * Update gbp.conf for pristine-tar usage now that we're no longer multi-orig
  * Remove "/var/lib/docker" upon purge (Closes: #739257)

  [ Dmitry Smirnov ]
  * Add support for DEB_BUILD_OPTIONS=nocheck in debian/rules

 -- Tianon Gravi <tianon@debian.org>  Mon, 11 Jul 2016 22:09:01 -0700

docker.io (1.11.2~ds1-3) unstable; urgency=medium

  * Team upload.
  * Updated "skip-privileged-unit-tests.patch" to skip more privileged
    tests in order to fix FTBFS in pbuilder.
  * Install "opts" directory to -dev package.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 09 Jul 2016 13:49:02 +1000

docker.io (1.11.2~ds1-2) unstable; urgency=medium

  * Add Tim Potter (tpot) and Dmitry Smirnov (onlyjob) to debian/copyright; they
    were instrumental in getting 1.11 into the archive!
  * Fix golang-github-docker-docker-dev install location (Closes: #830478);
    thanks nicoo!

 -- Tianon Gravi <tianon@debian.org>  Fri, 08 Jul 2016 08:47:44 -0700

docker.io (1.11.2~ds1-1) unstable; urgency=medium

  * Update to 1.11.2 upstream release
    (Closes: #806887, #820149, #822628, #812838)
  * Add NEWS file describing the AUFS issue and the unfortunate possible
    "solutions" (Closes: #799386, #805725)
  * Add "/etc/docker" to the directories explicitly installed by the package
    to help combat issues like #806261
  * Update "Homepage" to "dockerproject.org" (versus ".com" which now redirects)
  * Update "Vcs-Browser" to use https
  * Shrink the Ubuntu delta by pulling in many of the changes
  * Replace "btrfs-tools" relations with "btrfs-progs" (Closes: #824833)
  * Adjust "repack.sh" to allow keeping minor bits of vendor/
  * Fix bad URL in README (Closes: #816844); thanks Clint!
  * Move documentation to dedicated "docker-doc" package
  * Refresh patches, add minor patch to get unit tests running
  * Use gccgo on non-golang architectures (Closes: #785093)
  * Use "dh-golang" to calculate "Built-Using" more accurately
  * Add simple "basic-smoke" DEP8 test

 -- Tianon Gravi <tianon@debian.org>  Mon, 04 Jul 2016 09:59:44 -0700

docker.io (1.8.3~ds1-2) unstable; urgency=medium

  * Move "overlay" higher in priority (Closes: #799087)
  * Adjust "native.cgroupdriver" to default to "cgroupfs" (Closes: #798778)

 -- Tianon Gravi <tianon@debian.org>  Wed, 04 Nov 2015 00:09:02 -0800

docker.io (1.8.3~ds1-1) unstable; urgency=medium

  * Update to 1.8.3 upstream release (CVE-2014-8178, CVE-2014-8179)

 -- Tianon Gravi <tianon@debian.org>  Thu, 29 Oct 2015 19:40:51 -0700

docker.io (1.8.2~ds1-2) unstable; urgency=medium

  * Swap Build-Depends order to appease buildds (Closes: #803136)

 -- Tianon Gravi <tianon@debian.org>  Thu, 29 Oct 2015 07:23:10 -0700

docker.io (1.8.2~ds1-1) unstable; urgency=medium

  * Update to 1.8.2 upstream release
  * Rename golang-docker-dev package to golang-github-docker-docker-dev
  * Add SELinux support (Closes: #799620)

 -- Tianon Gravi <tianon@debian.org>  Wed, 28 Oct 2015 14:21:00 -0700

docker.io (1.7.1~dfsg1-1) unstable; urgency=medium

  * Update to 1.7.1 upstream release
  * Remove patches applied upstream; refresh other patches
  * Update Build-Depends

 -- Tianon Gravi <tianon@debian.org>  Wed, 26 Aug 2015 10:13:48 -0700

docker.io (1.6.2~dfsg1-2) unstable; urgency=medium

  * Add DEP8 tests
    - integration: runs upstream's integration tests
  * Replace "code.google.com/p/go.net" with canonical "golang.org/x/net"
    (Closes: #789736)

 -- Tianon Gravi <admwiggin@gmail.com>  Wed, 01 Jul 2015 07:45:19 -0600

docker.io (1.6.2~dfsg1-1) unstable; urgency=medium

  * Update to 1.6.2 upstream release
  * Update deps in d/control to match upstream's hack/vendor.sh specifications

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 21 May 2015 00:47:43 -0600

docker.io (1.6.1+dfsg1-2) unstable; urgency=medium

  * Add --no-restart-on-upgrade to dh_installinit so that we don't force
    a stop on upgrade, which can cause other units to fall over. Many thanks
    to Michael Stapelberg (sECuRE) for the tip!

 -- Paul Tagliamonte <paultag@debian.org>  Sun, 10 May 2015 13:02:54 -0400

docker.io (1.6.1+dfsg1-1) unstable; urgency=high

  * Update to 1.6.1 upstream release (Closes: #784726)
    - CVE-2015-3627
      Insecure opening of file-descriptor 1 leading to privilege escalation
    - CVE-2015-3629
      Symlink traversal on container respawn allows local privilege escalation
    - CVE-2015-3630
      Read/write proc paths allow host modification & information disclosure
    - CVE-2015-3631
      Volume mounts allow LSM profile escalation

 -- Tianon Gravi <admwiggin@gmail.com>  Fri, 08 May 2015 17:57:10 -0600

docker.io (1.6.0+dfsg1-1) unstable; urgency=medium

  * Upload to unstable
  * Backport PR 12943 to support golang-go-patricia 2.*
  * Remove convenience copies of cgroupfs-mount in init.d / upstart scripts
    (Re: #783143)

 -- Tianon Gravi <admwiggin@gmail.com>  Tue, 05 May 2015 15:10:49 -0600

docker.io (1.6.0+dfsg1-1~exp1) experimental; urgency=medium

  * Update to 1.6.0 upstream release
  * Adjust "repack.sh" to be more tolerant of "dfsg" suffixes

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 16 Apr 2015 18:00:21 -0600

docker.io (1.6.0~rc7~dfsg1-1~exp1) experimental; urgency=low

  * Update to 1.6.0-rc7 upstream release

 -- Tianon Gravi <admwiggin@gmail.com>  Wed, 15 Apr 2015 19:35:46 -0600

docker.io (1.6.0~rc4~dfsg1-1) experimental; urgency=low

  [ Tianon Gravi ]
  * Update to 1.6.0-rc4 upstream release
    - drop golang 1.2 support (no longer supported upstream)
    - update Homepage to https://dockerproject.com
    - add check-config.sh to /usr/share/docker.io/contrib
    - add "distribution" as a new multitarball orig
    - backport auto "btrfs_noversion" patch from
      https://github.com/docker/docker/pull/12048
      (simplifying our logic for detecting whether to use it)
    - switch from dh-golang to direct install since we're not actually using the
      features it offers (due to upstream's build system)
    - enable "docker.service" on boot by default for restart policies to work

  [ Felipe Sateler ]
  * Add Built-Using for glibc (Closes: #769351).

 -- Tianon Gravi <admwiggin@gmail.com>  Mon, 06 Apr 2015 17:11:33 -0600

docker.io (1.5.0~dfsg1-1) experimental; urgency=low

  * Update to 1.5.0 upstream release (Closes: #773495)
  * Remove several patches applied upstream!
    - 9637-fix-nuke-bashism.patch
    - enable-non-amd64-arches.patch
  * Fix btrfs-tools handling to allow for building with btrfs-tools < 1.16.1

 -- Tianon Gravi <admwiggin@gmail.com>  Tue, 10 Mar 2015 22:58:49 -0600

docker.io (1.3.3~dfsg1-2) unstable; urgency=medium

  * Add fatal-error-old-kernels.patch to make Docker refuse to start on old,
    unsupported kernels (Closes: #774376)
  * Fix dh_auto_clean to clean up after the build properly, especially to avoid
    FTBFS when built twice (Closes: #774482)

 -- Tianon Gravi <admwiggin@gmail.com>  Sat, 03 Jan 2015 00:11:47 -0700

docker.io (1.3.3~dfsg1-1) unstable; urgency=medium

  [ Tianon Gravi ]
  * Update to 1.3.3 upstream release (Closes: #772909)
    - Fix for CVE-2014-9356 (Path traversal during processing of absolute
      symlinks)
    - Fix for CVE-2014-9357 (Escalation of privileges during decompression of
      LZMA (.xz) archives)
    - Fix for CVE-2014-9358 (Path traversal and spoofing opportunities presented
      through image identifiers)
  * Fix bashism in nuke-graph-directory.sh (Closes: #772261)

  [ Didier Roche ]
  * Support starting systemd service without /etc/default/docker
    (Closes: #770293)

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 18 Dec 2014 21:54:12 -0700

docker.io (1.3.2~dfsg1-1) unstable; urgency=high

  * Severity is set to high due to the sensitive nature of the CVEs this
    upload fixes.
  * Update to 1.3.2 upstream release
    - Fix for CVE-2014-6407 (Archive extraction host privilege escalation)
    - Fix for CVE-2014-6408 (Security options applied to image could lead
                             to container escalation)
  * Remove Daniel Mizyrycki from Uploaders. Thanks for all your work!

 -- Paul Tagliamonte <paultag@debian.org>  Mon, 24 Nov 2014 19:14:28 -0500

docker.io (1.3.1~dfsg1-2) unstable; urgency=medium

  * Remove deprecated /usr/bin/docker.io symlink
    - added as a temporary shim in 1.0.0~dfsg1-1 (13 Jun 2014)
    - unused by package-installed files in 1.2.0~dfsg1-1 (13 Sep 2014)

 -- Tianon Gravi <admwiggin@gmail.com>  Fri, 07 Nov 2014 13:11:34 -0700

docker.io (1.3.1~dfsg1-1) unstable; urgency=high

  * Update to 1.3.1 upstream release
    - fix for CVE-2014-5277
    - https://groups.google.com/d/topic/docker-user/oYm0i3xShJU/discussion

 -- Tianon Gravi <admwiggin@gmail.com>  Mon, 03 Nov 2014 08:26:29 -0700

docker.io (1.3.0~dfsg1-1) unstable; urgency=medium

  * Updated to 1.3.0 upstream release.
  * Enable systemd socket activation (Closes: #752555).

 -- Tianon Gravi <admwiggin@gmail.com>  Fri, 17 Oct 2014 00:56:07 -0600

docker.io (1.2.0~dfsg1-2) unstable; urgency=medium

  * Added "golang-docker-dev" package for the reusable bits of Docker's source.

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 09 Oct 2014 00:08:11 +0000

docker.io (1.2.0~dfsg1-1) unstable; urgency=medium

  * Updated to 1.2.0 upstream release (Closes: #757183, #757023, #757024).
  * Added upstream man pages.
  * Updated bash and zsh completions to be installed as "docker" and "_docker".
  * Updated init scripts to also be installed as "docker".
  * Fixed "equivalent" typo in README.Debian (Closes: #756395). Thanks Reuben!
  * Removed "docker.io" mention in README.Debian (Closes: #756290). Thanks
    Olivier!

 -- Tianon Gravi <admwiggin@gmail.com>  Sat, 13 Sep 2014 11:43:17 -0600

docker.io (1.0.0~dfsg1-1) unstable; urgency=medium

  * Updated to 1.0.0 upstream release. Huzzah!
  * I've removed what is commonly called a `button' of patches against
    the docker package. Exact patches:
     - bash-completion-docker.io.patch
     - systemd-docker.io.patch
     - sysvinit-provides-docker.io.patch
     - zsh-completion-docker.io.patch
     - mkimage-docker.io.patch
  * I know y'all are guessing why; and the answer's pretty simple -- we're
    no longer docker.io(1). Since the src:docker package now ships wmdocker(1),
    we can safely declare a breaks/replaces on the pre-wmdocker version of the
    package, allowing existing users to safely update, both src:docker and
    src:docker.io side. This brings us into line with other distros, which
    now ship wmdocker(1) and docker(1).
  * As a stop-gap, I'm still shipping a docker.io(1) symlink to allow
    migration away.

 -- Paul Tagliamonte <paultag@debian.org>  Fri, 13 Jun 2014 21:04:53 -0400

docker.io (0.11.1~dfsg1-1) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Use EnvironmentFile with the systemd unit file. (Closes: #746774)
  * Patch out version checking code. (Closes: #747140)
  * Remove all host checking for non-amd64 host arches. Let docker build
    and run on all platforms now. (Closes: #747139, #739914)

  [ Tianon Gravi ]
  * Updated to 0.11.1 upstream release.
  * Added backported upstream patch for removing RemoteAddr assumptions
    that cause events to not be delivered to more than one unix socket
    listener.

 -- Tianon Gravi <admwiggin@gmail.com>  Fri, 09 May 2014 17:30:45 -0400

docker.io (0.9.1~dfsg1-2) unstable; urgency=medium

  * Added upstream apparmor patch to fix newer apparmor versions (such as the
    version appearing in Ubuntu 14.04).
  * Added mkimage-* docker.io binary name patches (Closes: #740855).

 -- Tianon Gravi <admwiggin@gmail.com>  Tue, 08 Apr 2014 23:19:08 -0400

docker.io (0.9.1~dfsg1-1) unstable; urgency=medium

  * Updated to 0.9.1 upstream release (Closes: #743424).
  * Added cgroupfs-mount dependency (Closes: #742641).
  * Added Suggests entries for optional features, chiefly lxc (Closes: #742081).
  * Added notes about "root-equivalence" to README.Debian (Closes: #742387).

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 03 Apr 2014 21:38:30 -0400

docker.io (0.9.0+dfsg1-1) unstable; urgency=medium

  * Updated README.Debian to not be quite so outdated (Closes: #740850).
  * Updated to 0.9.0 upstream release.

 -- Tianon Gravi <admwiggin@gmail.com>  Tue, 11 Mar 2014 22:24:31 -0400

docker.io (0.8.1+dfsg1-1) unstable; urgency=medium

  * Updated to 0.8.1 upstream release.

 -- Tianon Gravi <admwiggin@gmail.com>  Tue, 25 Feb 2014 20:56:31 -0500

docker.io (0.8.0+dfsg1-2) unstable; urgency=medium

  [ Tianon Gravi ]
  * Added more license notes to debian/copyright (Closes: #738627).

 -- Tianon Gravi <admwiggin@gmail.com>  Sat, 15 Feb 2014 17:51:58 -0500

docker.io (0.8.0+dfsg1-1) unstable; urgency=medium

  [ Prach Pongpanich ]
  * Added zsh completion.

  [ Tianon Gravi ]
  * Updated to 0.8.0 upstream release.
  * Added vim syntax files in new vim-syntax-docker package.
  * Added note about minimum recommended kernel version to Description.
  * Added contrib/*-integration files in /usr/share/docker.io/contrib.

 -- Tianon Gravi <admwiggin@gmail.com>  Mon, 10 Feb 2014 20:41:10 -0500

docker.io (0.7.6+dfsg1-1) unstable; urgency=medium

  [ Johan Euphrosine ]
  * Updated to 0.7.6.
  * Added dependency to gocapability.
  * Clean patches.

  [ Tianon Gravi ]
  * Added contrib/mk* scripts from upstream into /usr/share/docker.io/contrib
    (Closes: #736068).
  * Added upstream udev rules file to stop device-mapper devices and mounts from
    appearing in desktop environments through udisks.

 -- Johan Euphrosine <proppy@google.com>  Wed, 22 Jan 2014 22:50:47 -0500

docker.io (0.7.1+dfsg1-1) unstable; urgency=medium

  [ Prach Pongpanich ]
  * Fixed "docker: command not found" errors while using bash tab completion
    (Closes: #735372).

  [ Tianon Gravi ]
  * Updated to 0.7.1 upstream release (while we wait for gocapability to be
    packaged).
  * Added xz-utils recommend which is required for decompressing certain images
    from the index.

 -- Tianon Gravi <admwiggin@gmail.com>  Wed, 15 Jan 2014 20:22:34 -0500

docker.io (0.6.7+dfsg1-3) unstable; urgency=medium

  * Fixed FTBFS on non-amd64 platforms by setting the correct GOPATH.
  * Fixed issues with Docker finding a valid dockerinit (Closes: #734758).
  * Added aufs-tools dependency.

 -- Tianon Gravi <admwiggin@gmail.com>  Thu, 09 Jan 2014 20:10:20 -0500

docker.io (0.6.7+dfsg1-2) unstable; urgency=medium

  * Added iptables dependency required for Docker to start.
  * Added ca-certificates recommend required for pulling from the index.

 -- Tianon Gravi <admwiggin@gmail.com>  Wed, 08 Jan 2014 19:14:02 -0500

docker.io (0.6.7+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #706060, #730569)
  * Document missing licenses in the source tree. Bad, paultag. Thanks
    alteholz.

 -- Paul Tagliamonte <paultag@debian.org>  Tue, 07 Jan 2014 21:06:10 -0500
